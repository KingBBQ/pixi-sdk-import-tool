# Sample App for importing items

this app is a little demo, how items in a CSV can be imported into pixi. 

First, choose the file: 
![Choose File](https://bitbucket.org/repo/xznjEL/images/603571347-Bildschirmfoto%202016-01-08%20um%2018.29.20.png)



Second, see the items to be imported: 
![Preview before import](https://bitbucket.org/repo/xznjEL/images/4032735644-Bildschirmfoto%202016-01-08%20um%2018.29.13.png)



Third, the items get's into the channels in pixi:
![Overview of XMLS ins pixi](https://bitbucket.org/repo/xznjEL/images/1245784941-Bildschirmfoto%202016-01-08%20um%2018.29.03.png)



And finally, the items are in: 
![Result in pixi](https://bitbucket.org/repo/xznjEL/images/1378524444-Bildschirmfoto%202016-01-08%20um%2018.37.45.png)



# Flat File Order Analyzer

This sample takes a CSV file, you choose the columns that hold important data, and it does some simple analysis. This can help both on sales an installations. 

## Choose the Columns from the CSV that matter...

![Choose the colums](https://bitbucket.org/repo/5BRnXB/images/1917663721-Bildschirmfoto%202016-07-13%20um%2012.47.53.png)

## The Result. 

![Order Analysis](https://bitbucket.org/repo/5BRnXB/images/3868681498-Bildschirmfoto%202016-07-13%20um%2012.50.39.png)