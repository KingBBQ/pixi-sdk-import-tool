<form enctype="multipart/form-data" action="<?php echo site_url();?>/<?php echo $url ?>/upload" method="POST">
<div class="widget-body">
				<div class="widget-main no-padding">
					<form>
						<!-- <legend>Form</legend> -->
						<fieldset>
							<label>Diese Datei hochladen:</label>


							
    <!-- Der Name des Input Felds bestimmt den Namen im $_FILES Array -->
     <input name="userfile" type="file" />
    					</fieldset>

						<div class="form-actions center">
                                <button type="submit" class="btn btn-primary" id="form-field-submit">
                                    <i class="icon-shopping-cart bigger-110"></i> <span>Hochladen</span>
                                </button>


							</div>
					</form>
				</div>
			</div>

<!-- Die Encoding-Art enctype MUSS wie dargestellt angegeben werden -->
    <!-- MAX_FILE_SIZE muss vor dem Dateiupload Input Feld stehen -->

</form>
