<?php


use Pixi\Ui\Table\Table;
use Pixi\Ui\Data\DataFormat;
use Pixi\Ui\Chart\Chart;
use Pixi\Ui\Dropdown\PaymentsDropdown;
use Pixi\Ui\Form\Form;
use Pixi\Ui\Form\FormElement;
use Pixi\Ui\Info\Info;
use Pixi\Ui\Info\InfoElement;
use Pixi\Ui\DataList\DataList;
use Pixi\Ui\Timeline\Timeline;
use Pixi\Ui\Timeline\TimelineElement;


class Analyse extends SDKMenu

{
  private function increase(&$var, $inc = 1) {
      if(!(isset($var)))  $var = 0;
      $var = $var + $inc;
  }

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $html = $this->load->view('import_upload', array('url' => "orderimport"), true);
        $this->loadMainView('Bestellimport', 'Bitte Datei zur Bearbeitung auswählen. ', $html);
    }


    public function upload()
{


        $template_item = '';
        // Get table content

        $csv = array();


        if (($handle = fopen($_FILES['userfile']['tmp_name'], "r")) !== FALSE) {
            while (($data = fgetcsv($handle, null, ",",'"')) !== FALSE) {
              if (isset($data[0]))
              $csv[] = array(
                'OrderID' => $data[0],
                'Name' => $data[1],
                'Company' => $data[2],
                'Address' => $data[3],
                'Country' => $data[4],
                'ZIP' => $data[5],
                'City' => $data[6],
                'eMail' => $data[8],
                'phone' => $data[9],
                'OrderDate' => $data[10]. ' '. $data[17],
                'Shop_Note' => $data[11],
                'ShippingCost' => $data[15],
                'Items_Quantity' => $data[22],
                'Items_SKUs' => $data[20],
                'Items_Names' => $data[21],
                'Items_Prices' => $data[23],
              );
            }
            fclose($handle);
          }


          //var_dump($orders);
        /*
        $countryTable = new Table('Vorschau der Bestellungen', Table::TableTypeDataTables);
        $countryTable->addColumn('OrderID', 'ID', DataFormat::FORMAT_STRING);
        $countryTable->addColumn('OrderDate', 'Datum', DataFormat::FORMAT_STRING);
        /*$countryTable->addColumn('Name', 'Name', DataFormat::FORMAT_STRING);
        $countryTable->addColumn('Address', 'Adresse', DataFormat::FORMAT_STRING);
        $countryTable->addColumn('ZIP', 'PLZ', DataFormat::FORMAT_STRING);
        $countryTable->addColumn('City', 'Stadt', DataFormat::FORMAT_STRING);
        $countryTable->addColumn('eMail', 'eMail', DataFormat::FORMAT_STRING);
        $countryTable->addColumn('Items_Quantity', 'Anz.', DataFormat::FORMAT_STRING);
        $countryTable->addColumn('Items_SKUs', 'EAN', DataFormat::FORMAT_STRING);
        $countryTable->addColumn('Items_Names', 'Artikel', DataFormat::FORMAT_STRING);
        $countryTable->addColumn('Items_Prices', 'Preis', DataFormat::FORMAT_STRING);

        // After the table structure was defined, we assign the content to the table
        $countryTable->addRows($csv);
        */

        $data['file']  = $_FILES['userfile']['tmp_name'];
        $data['filename']  = $_FILES['userfile']['name'];


        // take the CSV and move it into 2 differnt arrays:

        $orders = array();
        $items  = array();
        $OneItemOrdersCount = 0;
        $QtySum = 0; // the sum of all QTY! So QTY,

        foreach ($csv as $item) {
          //var_dump($item);

          // not needed, if there is one line per order, so comment out for now.
          /*
          if (!isset($order[$item['OrderID']])) {
            $order[$item['OrderID']] = array();
          }
          */
          $OrderID = $item['OrderID'];

          $orders[$OrderID] = $item;



          $orders[$OrderID]['Items'] = array();
          $Items_Quantity   = explode('|', $item['Items_Quantity']);
          $Items_SKUs       = explode('|', $item['Items_SKUs']);
          $Items_Names      = explode('|', $item['Items_Names']);
          $Items_Prices     = explode('|', $item['Items_Prices']);

          //var_dump($Items_Prices);
          $itemcount = 0;
          for ($i = 0; $i < count($Items_Quantity); $i++) {

            $orders[$OrderID]['Items'][] =
              array(
                'Qty' => $Items_Quantity[$i],
                'SKU' => $Items_SKUs[$i],
                'Name' => $Items_Names[$i],
                'Price' => $Items_Prices[$i],
              );
              $itemcount = $itemcount + $Items_Quantity[$i];

              /*$items[$Items_SKUs[$i]][] = array(
                'OrderID' => $OrderID,
                'Qty'     => $Items_Quantity[$i],
              );*/

              $this->increase($items[$Items_SKUs[$i]]['Count'], $Items_Quantity[$i]);
              $items[$Items_SKUs[$i]]['Name'] = $Items_Names[$i];
              $items[$Items_SKUs[$i]]['SKU'] = $Items_SKUs[$i];
              $items[$Items_SKUs[$i]]['Price'] = $Items_Prices[$i];

              $QtySum = $QtySum + $Items_Quantity[$i];

          }

          if ($itemcount == 1) {
              $orders[$OrderID]['1SS'] = 1;
              $OneItemOrdersCount++;

              $this->increase($items[$item['Items_SKUs']]['1SSCount']);

            }
          else $orders[$OrderID]['1SS'] = 0;
        }


        // Sort by the Count.
        function cmp($a, $b)
        {
            return ($a["Count"] < $b["Count"]);
        }

        usort($items, "cmp");

        // doing the ABC... (this is not a dance!)

        $A = 0.8;
        $B = 0.15;

        $QtyA = 0;
        $QtyB = 0;
        $QtyC = 0;
        foreach ($items as $key => $item)
        {
          //var_dump($item);
          $items[$key]['Percentange'] = $item['Count'] / $QtySum * 100;


          $this->increase($SumUntilNow, $item['Count']);

          if (($SumUntilNow / $QtySum) < $A) {
              $items[$key]['ABC'] = 'A';
              $QtyA++;
            }
          elseif (($SumUntilNow / $QtySum) < ($A+$B)) {
            $items[$key]['ABC'] = 'B';
            $QtyB++;
          }
          else {
            $items[$key]['ABC'] = 'C';
            $QtyC++;
          };

          $items[$key]['SumSorted'] = $SumUntilNow;
        }


        //var_dump($items);
        $itemsTable = new Table('Artikel', 'Artikels');
        $itemsTable->addColumn('SKU', 'ID', DataFormat::FORMAT_STRING);
        $itemsTable->addColumn('Name', 'Name', DataFormat::FORMAT_STRING);
        $itemsTable->addColumn('Count', 'Menge', DataFormat::FORMAT_NUMBER);
        $itemsTable->addColumn('1SSCount', '1SS Menge', DataFormat::FORMAT_NUMBER);
        $itemsTable->addColumn('Price', 'Price', DataFormat::FORMAT_MONEY);
        $itemsTable->addColumn('Percentange', '%', DataFormat::FORMAT_NUMBER);
        $itemsTable->addColumn('ABC', '%', DataFormat::FORMAT_STRING);
        $itemsTable->addRows($items);




        $Overview = new Info('Überblick der Bestellungen');

        // Add Info
        $Overview->addElement(new InfoElement('Artikel', 'Anz: '.count($items).' Menge:'.$QtySum, 'fa-barcode', 'green', ''));

        $Overview->addElement(new InfoElement('Bestellungen', 'Anz: '.count($orders), 'fa-inbox', 'green', ''));

        $Overview->addElement(new InfoElement('1-Scan', 'Anz: '.$OneItemOrdersCount.' ('.(round(($OneItemOrdersCount/count($orders)*100),2)).'% )', 'fa-rocket', 'red', ''));


        $ABC = new Info('ABC Analyse der Bestellungen');

        $ABC->addElement(new InfoElement('A '.($A*100).' %', 'Anz: '.$QtyA, 'fa-heart', 'green', ''));
        $ABC->addElement(new InfoElement('B'.($B*100).' %', 'Anz: '.$QtyB, 'fa-question', 'orange', ''));
        $ABC->addElement(new InfoElement('C', 'Anz: '.$QtyC, 'fa-flag', 'red', ''));



        $this->loadMainView('Bestellimport - '.$_FILES['userfile']['name'], 'Preview of the File to be imported', array( $Overview, $ABC, $itemsTable));



    }

    public function getChannels() {
        var_dump($this->pixiapi->pixiImportGetChannels());
    }
}
