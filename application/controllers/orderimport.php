<?php


use Pixi\Ui\Table\Table;
use Pixi\Ui\Data\DataFormat;
use Pixi\Ui\Chart\Chart;
use Pixi\Ui\Dropdown\PaymentsDropdown;
use Pixi\Ui\Form\Form;
use Pixi\Ui\Form\FormElement;
use Pixi\Ui\Info\Info;
use Pixi\Ui\Info\InfoElement;
use Pixi\Ui\DataList\DataList;
use Pixi\Ui\Timeline\Timeline;
use Pixi\Ui\Timeline\TimelineElement;


class Orderimport extends SDKMenu

{
  private function increase(&$var, $inc = 1) {
      if(!(isset($var)))  $var = 0;
      $var = $var + $inc;
  }

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $html = $this->load->view('import_upload', array('url' => "orderimport"), true);
        $this->loadMainView('Bestellimport', 'Bitte Datei zur Bearbeitung auswählen. ', $html);
    }


    public function generateHeader($order) {

      $XML = '<ORDER version="1.0" type="standard">
      <ORDER_HEADER>
      <CONTROL_INFO>
      <GENERATOR_INFO/>
      <GENERATION_DATE>'.date('Y-m-d H:i:s').'</GENERATION_DATE>
      <DATABASE>pixi_ANC</DATABASE>
      <SHOPID>ANC</SHOPID>
      </CONTROL_INFO>
      <ORDER_INFO>
      <ORDER_ID>'.$order['OrderID'].'</ORDER_ID>
      <ORDER_DATE>'.$order['OrderDate'].'</ORDER_DATE>
      <DATABASE>pixi_ANC</DATABASE>
      <SHOPID>ANC</SHOPID>
      <ORDER_TYPE>B2C</ORDER_TYPE>
      <SHOP_NOTE/>
      <ORDER_SHIPLOCK>N</ORDER_SHIPLOCK>
      <ORDER_PARTIES>
      <BUYER_PARTY>
      <PARTY>
      <PARTY_ID type="buyer_specific">'.$order['eMail'].'</PARTY_ID>
      <ADDRESS>
      <SAL></SAL>
      <NAME>'.$order['Name'].'</NAME>
      <NAME2/>
      <NAME3>'.$order['Company'].'</NAME3>
      <STREET>'.$order['Address'].'</STREET>
      <ZIP>'.$order['ZIP'].'</ZIP>
      <ZIPBOX/>
      <CITY>'.$order['City'].'</CITY>
      <COUNTRY>'.$order['Country'].'</COUNTRY>
      <VAT_ID/>
      <PHONE>'.$order['Phone'].'</PHONE>
      <EMAIL>'.$order['eMail'].'</EMAIL>
      </ADDRESS>
      </PARTY>
      </BUYER_PARTY>
      <INVOICE_PARTY>
      <PARTY>
      <PARTY_ID type="buyer_specific">'.$order['eMail'].'</PARTY_ID>
      <ADDRESS>
      <SAL></SAL>
      <NAME>'.$order['Name'].'</NAME>
      <NAME2/>
      <NAME3>'.$order['Company'].'</NAME3>
      <STREET>'.$order['Address'].'</STREET>
      <ZIP>'.$order['ZIP'].'</ZIP>
      <ZIPBOX/>
      <CITY>'.$order['ZIP'].'</CITY>
      <COUNTRY>'.$order['Country'].'</COUNTRY>
      <VAT_ID/>
      <PHONE>'.$order['Phone'].'</PHONE>
      <EMAIL>'.$order['eMail'].'</EMAIL>
      </ADDRESS>
      </PARTY>
      </INVOICE_PARTY>
      <SHIPMENT_PARTIES>
      <DELIVERY_PARTY>
      <PARTY>
      <ADDRESS>
      <SAL></SAL>
      <NAME>'.$order['Name'].'</NAME>
      <NAME2/>
      <NAME3>'.$order['Company'].'</NAME3>
      <STREET>'.$order['Address'].'</STREET>
      <ZIP>'.$order['ZIP'].'</ZIP>
      <ZIPBOX/>
      <CITY>'.$order['ZIP'].'</CITY>
      <COUNTRY>'.$order['Country'].'</COUNTRY>
      <VAT_ID/>
      <PHONE>'.$order['Phone'].'</PHONE>
      <ADDRESS_REMARKS/>
      <VAT_ID/>
      <CHARGE_VAT>Y</CHARGE_VAT>
      </ADDRESS>
      </PARTY>
      </DELIVERY_PARTY>
      </SHIPMENT_PARTIES>
      </ORDER_PARTIES>
      <PRICE_CURRENCY>EUR</PRICE_CURRENCY>
      <EXCHANGE_RATE>1.000000000000</EXCHANGE_RATE>
      <PAYMENT>
      <'.$order['PaymentType'].'/>
      </PAYMENT>
      <REMARK type="discount">0.00</REMARK>
      <REMARK type="shipping">'.$order['ShippingCost'].'</REMARK>
      </ORDER_INFO>
      </ORDER_HEADER>';

      return $XML;
    }



    public function generateItem($items, $orderid) {

      $XML = '<ORDER_ITEM_LIST>
      ';

      foreach ($items as $item) {
        $XML .= '
        <ORDER_ITEM>
        <LINE_ITEM_ID>'.$orderid.'-'.$item['SKU'].'</LINE_ITEM_ID>
        <ARTICLE_ID>
        <SUPPLIER_AID>'.$item['SKU'].'</SUPPLIER_AID>
        <ITEM_NAME'.$item['Name'].'</ITEM_NAME>
        </ARTICLE_ID>
        <QUANTITY>'.$item['Qty'].'</QUANTITY>
        <DESCRIPTION_SHORT/>
        <ARTICLE_PRICE type="udp_gross_customer">
        <FULL_PRICE>'.$item['Price'].'</FULL_PRICE>
        </ARTICLE_PRICE>
        </ORDER_ITEM>
        ';
      };

      $XML .= '</ORDER_ITEM_LIST>
      <ORDER_SUMMARY>
      <TOTAL_ITEM_NUM>1</TOTAL_ITEM_NUM>
      </ORDER_SUMMARY>
      </ORDER>';
      return $XML;
    }


    public function upload()
{


        $template_item = '';
        // Get table content

        $csv = array();

/*
0 => string 'Order ID' (length=8)
1 => string 'Shipping: Full Name' (length=19)
2 => string 'Shipping: Company' (length=17)
3 => string 'Shipping: Street Address (Full)' (length=31)
4 => string 'Shipping: Country (prefix)' (length=26)
5 => string 'Shipping: ZIP Code' (length=18)
6 => string 'Shipping: City' (length=14)
7 => string 'Shipping: State (prefix)' (length=24)
8 => string 'Billing: E-mail Address' (length=23)
9 => string 'Billing: Phone Number' (length=21)
10 => string 'Order Date' (length=10)
11 => string 'Customer Message' (length=16)
12 => string 'Order Total' (length=11)
13 => string 'Payment Gateway ID' (length=18)
14 => string 'Shipping: State' (length=15)
15 => string 'Shipping Cost' (length=13)
16 => string 'Shipping Tax Total' (length=18)
17 => string 'Order Time' (length=10)
18 => string 'Paid date' (length=9)
19 => string 'Free order type' (length=15)
20 => string 'Order Items: SKU' (length=16)
21 => string 'Order Items: Product Name' (length=25)
22 => string 'Order Items: Quantity' (length=21)
23 => string 'Order Items: RRP' (length=16)
*/
        if (($handle = fopen($_FILES['userfile']['tmp_name'], "r")) !== FALSE) {
            while (($data = fgetcsv($handle, null, ",",'"')) !== FALSE) {
              if (isset($data[0]))
              $csv[] = array(
                'OrderID' => $data[0],
                'Name' => $data[1],
                'Company' => $data[2],
                'Address' => $data[3],
                'Country' => $data[4],
                'ZIP' => $data[5],
                'City' => $data[6],
                'eMail' => $data[8],
                'Phone' => $data[9],
                'OrderDate' => $data[10]. ' '. $data[17],
                'Shop_Note' => $data[11],
                'ShippingCost' => $data[15],
                'Items_Quantity' => $data[22],
                'Items_SKUs' => $data[20],
                'Items_Names' => $data[21],
                'Items_Prices' => $data[23],
                'ShippingCost' => $data[15],
                'Note' => $data[11],
                'PaymentType' => $data[13],
              );
              //var_dump($data); die;
            }
            fclose($handle);
          }


          //var_dump($orders);

        $orderTable = new Table('Vorschau der Bestellungen', Table::TableTypeDataTables);
        $orderTable->addColumn('OrderID', 'ID', DataFormat::FORMAT_STRING);
        $orderTable->addColumn('OrderDate', 'Datum', DataFormat::FORMAT_STRING);
        $orderTable->addColumn('Name', 'Name', DataFormat::FORMAT_STRING);
        $orderTable->addColumn('Address', 'Adresse', DataFormat::FORMAT_STRING);
        $orderTable->addColumn('ZIP', 'PLZ', DataFormat::FORMAT_STRING);
        $orderTable->addColumn('City', 'Stadt', DataFormat::FORMAT_STRING);
        $orderTable->addColumn('eMail', 'eMail', DataFormat::FORMAT_STRING);
        $orderTable->addColumn('Items_Quantity', 'Anz.', DataFormat::FORMAT_STRING);
        $orderTable->addColumn('Items_SKUs', 'EAN', DataFormat::FORMAT_STRING);
        $orderTable->addColumn('Items_Names', 'Artikel', DataFormat::FORMAT_STRING);
        $orderTable->addColumn('Items_Prices', 'Preis', DataFormat::FORMAT_STRING);

        // After the table structure was defined, we assign the content to the table
        $orderTable->addRows($csv);


        $data['file']  = $_FILES['userfile']['tmp_name'];
        $data['filename']  = $_FILES['userfile']['name'];


        // take the CSV and move it into 2 differnt arrays:

        $orders = array();
        $items  = array();
        $OneItemOrdersCount = 0;
        $QtySum = 0; // the sum of all QTY! So QTY,


        foreach ($csv as $item) {
          // not needed, if there is one line per order, so comment out for now.
          /*
          if (!isset($order[$item['OrderID']])) {
            $order[$item['OrderID']] = array();
          }
          */

          $XML = '';

          $OrderID = $item['OrderID'];
          $orders[$OrderID] = $item;


          $XML .= $this->generateHeader($item);


          $orders[$OrderID]['Items'] = array();
          $Items_Quantity   = explode('|', $item['Items_Quantity']);
          $Items_SKUs       = explode('|', $item['Items_SKUs']);
          $Items_Names      = explode('|', $item['Items_Names']);
          $Items_Prices     = explode('|', $item['Items_Prices']);


          for ($i = 0; $i < count($Items_Quantity); $i++) {

            $orders[$OrderID]['Items'][] =
              array(
                'Qty' => $Items_Quantity[$i],
                'SKU' => $Items_SKUs[$i],
                'Name' => $Items_Names[$i],
                'Price' => $Items_Prices[$i],
              );
              $QtySum = $QtySum + $Items_Quantity[$i];

          }

          $XML .= $this->generateItem($orders[$OrderID]['Items'],$OrderID);



          // Import the XML to pixi:
          $result_importxml = $this->pixiapi->pixiImportAddXML(array(
              'ChannelRef' => '2',
              'OperationType' => 'order',
              'ParameterXML' =>
              $XML));
          $xmlkey = $result_importxml->getResultset();
          var_dump($xmlkey);
die;
          $xmlkey = $xmlkey[0]['_x0058_MLKey'];

          $result_processXML = $this->pixiapi->pixiImportProcessXML(array('XMLLogKey' => $xmlkey));

          $result_processXML = $result_processXML->getResultset();

          var_dump($result_processXML);

          //  { ["ReturnCode"]=> string(2) "OK" ["ErrorMessage"]=> string(47) "XML with XMLLogKey 394009 has been reprocessed." } } }

          $this->addMessage($result_processXML[2][0]['ErrorMessage'], PixiController::MessageSuccess);

        }







        $Overview = new Info('Überblick der Bestellungen');

        // Add Info
        $Overview->addElement(new InfoElement('Artikel', 'Anz: '.count($items).' Menge:'.$QtySum, 'fa-barcode', 'green', ''));

        $Overview->addElement(new InfoElement('Bestellungen', 'Anz: '.count($orders), 'fa-inbox', 'green', ''));





        $this->loadMainView('Bestellimport - '.$_FILES['userfile']['name'], 'Preview of the File to be imported', array( $Overview, $orderTable));


        echo($XML);


    }

    public function getChannels() {
        var_dump($this->pixiapi->pixiImportGetChannels());
    }
}
