<?php

use Pixi\Ui\Table\Table;
use Pixi\Ui\Data\DataFormat;
use Pixi\Ui\Form\Form;
use Pixi\Ui\Form\FormElement;
use Pixi\Ui\Info\Info;
use Pixi\Ui\Info\InfoElement;

class carriers extends SDKMenu
{


    public function __construct()
    {
        parent::__construct();
    }

    function loadRulez() {
        #$picklist = $this->session->userdata('pl');
        $rulez = json_decode(file_get_contents('carriers.json'), true);
        if (!$rulez) $rulez = array();

        foreach ($rulez as $key => $rule){
          $rulez[$key]['ID'] = $key;
        }




        return $rulez;


    }

    function filterRulez($shopID = '') {
      $rulez = $this->loadRulez();
      if ($shopID != '') {
        $fileredRulez = array();
        // filter the results for just one shopID
        foreach ($rulez as $rule) {
          if ($rule['ShopID'] == $shopID)
          $fileredRulez[$rule['Country']][$rule['ZIP']] = $rule['Carrier'];
        }
        return $fileredRulez;


    }
  }


    function saveRulez($rulez) {
        #$this->session->set_userdata(array(
        #    'pl' => $pl
        #));

        file_put_contents("carriers.json",json_encode($rulez));


    }


    public function index()
    {

         $rulez = $this->loadRulez();

         if (isset($_GET['Carrier'])) {
            $formData = $_GET;
            //$formData['ID'] = count($rulez) + 1;
            //$rulez = array();
            $rulez[] = $formData;
            //var_dump($rulez);
            $this->saveRulez($rulez);
            //die;
         }


        // list all rulez up to now.
        $rulezTable = new Table('Bestehende Regeln');
        $rulezTable->addColumn('ShopID', 'Shop', DataFormat::FORMAT_STRING);
        $rulezTable->addColumn('Country', 'Land', DataFormat::FORMAT_STRING);
        $rulezTable->addColumn('ZIP', 'PLZ', DataFormat::FORMAT_STRING);
        $rulezTable->addColumn('Carrier', 'Carrier', DataFormat::FORMAT_STRING);
        $rulezTable->enableDeleteLink('carriers/delete', 'ID');
        $rulezTable->addRows($rulez);

        // form to add new carriers
        $addForm = new Form('carriers', 'GET', 'Neue Regel speichern');
        $addForm->addElement('ShopID', FormElement::ElementTypeString, 'Shop:');
        $addForm->addElement('Country', FormElement::ElementTypeString, 'Land (ISO):');
        $addForm->addElement('ZIP', FormElement::ElementTypeString, 'PLZ (Beginn):');
        $addForm->addElement('Carrier', FormElement::ElementTypeString, 'Carrier:');



        // form to add new carriers
        $testForm = new Form('carriers/test', 'GET', 'Regel testen');
        $testForm->addElement('ShopID', FormElement::ElementTypeString, 'Shop:');
        $testForm->addElement('Country', FormElement::ElementTypeString, 'Land (ISO):');
        $testForm->addElement('ZIP', FormElement::ElementTypeString, 'PLZ (Beginn):');


        $this->loadMainView('Carrier Rules', 'Set carriers based on Shop, Country and ZIP Code', array($rulezTable,'<h1>Neue Regel hinzufügen</h1>', $addForm, '<h1>Bestehende Regeln testen</h1>', $testForm));


    }


    public function delete ($ID) {

        $rulez = $this->loadRulez();
        unset($rulez[$ID]);
        $this->saveRulez($rulez);
        redirect('/carriers', 'refresh');
    }

    public function matchRule ($shopID, $Country, $ZIP) {
      $rulez = $this->filterRulez($_GET['ShopID']);

      //var_dump($rulez);


      if (isset($rulez[$Country][$ZIP])) {
        return $rulez[$Country][$ZIP];
      }

      //var_dump($rulez[$Country]);
      if (isset($rulez[$Country]))
      foreach ($rulez[$Country] as $key => $carrier) {
        if (($key != '') AND ($key == substr($ZIP, 0, strlen($key)))) {
          return $carrier;
        }
      }

      if (isset($rulez[$Country][''])) {
        if ($rulez[$Country][''] != '') return $rulez[$Country][''];
      }

      return $rulez[''][''];

    }

    public function test() {
      $carrier =  $this->matchRule($_GET['ShopID'], $_GET['Country'], $_GET['ZIP']);

      $this->addMessage('Carrier set by rules: '.$carrier, PixiController::MessageSuccess);

      $this->index();


      // go from specific to less specific
      // check for country / full ZIP first.







    }


}
