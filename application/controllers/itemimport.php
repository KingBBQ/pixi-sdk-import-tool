<?php


use Pixi\Ui\Table\Table;
use Pixi\Ui\Data\DataFormat;
use Pixi\Ui\Chart\Chart;
use Pixi\Ui\Dropdown\PaymentsDropdown;
use Pixi\Ui\Form\Form;
use Pixi\Ui\Form\FormElement;
use Pixi\Ui\Info\Info;
use Pixi\Ui\Info\InfoElement;
use Pixi\Ui\DataList\DataList;
use Pixi\Ui\Timeline\Timeline;
use Pixi\Ui\Timeline\TimelineElement;


class Itemimport extends SDKMenu

{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $html = $this->load->view('import_upload', array('url' => "itemimport"), true);
        $this->loadMainView('Item Importer', 'Please choose and upload your CSV file. ', $html);
    }


    public function upload()
    {

        $template_item = '';
        // Get table content
        $file = file($_FILES['userfile']['tmp_name']);
        $lines = explode("\r", $file[0]);
        // Durchgehen des Arrays und Anzeigen des HTML-Quelltexts inkl. Zeilennummern
        foreach ($lines as $line_num => $line) {
            $line = explode("\t", $line);
            //var_dump(count($line));
            if (count($line) == 5) {
            $result[] = array(
                'ID' => $line[0],
                'EAN' => $line[1],
                'ItemName' => $line[2],
                'Price' => $line[3],
                'Supplier' => $line[4],
            );
            };
        }


        $countryTable = new Table('Preview if items to be imported', Table::TableTypeDataTables);
        $countryTable->addColumn('ID', 'ID', DataFormat::FORMAT_STRING);
        $countryTable->addColumn('EAN', 'Barcode', DataFormat::FORMAT_STRING);
        $countryTable->addColumn('ItemName', 'Item Name', DataFormat::FORMAT_STRING);
        $countryTable->addColumn('Price', 'Price', DataFormat::FORMAT_MONEY);
        $countryTable->addColumn('Supplier', 'Supplier', DataFormat::FORMAT_STRING);

        // After the table structure was defined, we assign the content to the table
        $countryTable->addRows($result);

        //$data['filename'] = 'test';

        $data['file']  = $_FILES['userfile']['tmp_name'];
        $data['filename']  = $_FILES['userfile']['name'];

        //$importBTN = $this->load->view('import_process',$data,true);





        $template_header = '<BMECAT version="1.2">
   <HEADER>
      <GENERATOR_INFO>OXID eShop Professional Edition BMECat Export</GENERATOR_INFO>
   <CATALOG>
      <LANGUAGE>1</LANGUAGE>
      <CATALOG_ID>4e96c808be6bf</CATALOG_ID>
      <CATALOG_VERSION/>
      <CATALOG_NAME> 13.05.2015 BMECat Export</CATALOG_NAME>
      <DATABASE>pixi_TEST</DATABASE>
      <SHOPID>ABC</SHOPID>
      <DATEEXPORT>2015-05-13 13:14:15</DATEEXPORT>
   </CATALOG>
   </HEADER>
   <T_NEW_CATALOG>
            ';

        $file = file($_FILES['userfile']['tmp_name']);
        $lines = explode("\r", $file[0]);

        foreach ($lines as $line_num => $line) {
            $line = explode("\t", $line);
            //var_dump($line);
            if (count($line) == 5) {

        $template_item .= '     <ARTICLE>
      <SUPPLIER_AID>'.$line[0].'</SUPPLIER_AID>
      <ARTICLE_DETAILS>
           <DESCRIPTION_SHORT>'.$line[2].'</DESCRIPTION_SHORT>
           <WEIGHT>0</WEIGHT>
	   <INTERNAL_ITEM_NUMBER>'.$line[0].'</INTERNAL_ITEM_NUMBER>
      </ARTICLE_DETAILS>
      <ARTICLE_FEATURES>
         <FEATURE>
            <FNAME>ID '.$line[4].'</FNAME>
            <FVALUE>'.$line[0].'</FVALUE>
         </FEATURE>
	 <FEATURE>
            <FNAME>Europe1PriceFactory.PTG</FNAME>
            <FVALUE>MWST_HIGH</FVALUE>
         </FEATURE>
      </ARTICLE_FEATURES>
      <ARTICLE_PRICE_DETAILS>
         <ARTICLE_PRICE price_type="gros_list">
            <PRICE_AMOUNT>10</PRICE_AMOUNT>
            <PRICE_CURRENCY>EUR</PRICE_CURRENCY>
         </ARTICLE_PRICE>
      </ARTICLE_PRICE_DETAILS>

      <!--optional-->
      <ITEMTAGS>
         <ITEMTAG>API-Import</ITEMTAG>
      </ITEMTAGS>
   </ARTICLE>
        ';
        };
        }

        $template_footer = '
            </T_NEW_CATALOG>
            </BMECAT>';

       // echo('<PRE>');
       // echo();
       // echo($template_item);
       // echo($template_footer);
       // echo('</PRE>');

        $xml_preview = '<textarea>'
            .$template_header
            .$template_item
            .$template_footer
        .'</textarea>';



        $result_importxml = $this->pixiapi->pixiImportAddXML(array(
            'ChannelRef' => '8',
            'OperationType' => 'item',
            'ParameterXML' =>
            $template_header
            .$template_item
            .$template_footer));

            $xmlkey = $result_importxml->getResultset()[0]['_x0058_MLKey'];

        $result_processXML = $this->pixiapi->pixiImportProcessXML(array('XMLLogKey' => $xmlkey));

        $result_processXML = $result_processXML->getResultset();

        //var_dump($result_processXML);

        //  { ["ReturnCode"]=> string(2) "OK" ["ErrorMessage"]=> string(47) "XML with XMLLogKey 394009 has been reprocessed." } } }

        $this->addMessage($result_processXML[2][0]['ErrorMessage'], PixiController::MessageSuccess);


        $this->loadMainView('Item Importer - '.$_FILES['userfile']['name'], 'Preview of the File to be imported', array($countryTable, $xml_preview));



    }

    public function getChannels() {
        var_dump($this->pixiapi->pixiImportGetChannels());
    }
}
