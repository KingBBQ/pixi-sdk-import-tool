<?php

use Pixi\Ui\Table\Table;
use Pixi\Ui\Data\DataFormat;
use Pixi\Ui\Form\Form;
use Pixi\Ui\Form\FormElement;
use Pixi\Ui\Info\Info;
use Pixi\Ui\Info\InfoElement;

class testimport extends SDKMenu
{
    // Must be tested with ===, as in if(isXML($xml) === true){}
  // Returns the error message on improper XML
  private function isXML($xml)
  {
      libxml_use_internal_errors(true);

      if ($xml == '') {
          return 'Empty Document';
      }

      $doc = new DOMDocument('1.0', 'utf-8');
      $doc->loadXML($xml);

      $errors = libxml_get_errors();

      if (empty($errors)) {
          return true;
      }

      $error = $errors[0];
      if ($error->level < 3) {
          return true;
      }

      $explodedxml = explode('r', $xml);
      $badxml = $explodedxml[($error->line) - 1];

      $message = $error->message.' at line '.$error->line.'. Bad XML: '.htmlentities($badxml);

      return $message;
  }

    private function prepareXML($URL)
    {
        $XML = $this->http_request($URL);
        $XML = preg_replace('/<\?[^>]*>/', '', $XML); // remove any <? ... XML Tags, PHP Parser doesnt like them.
        $XML = preg_replace('/<\![^>]*>/', '', $XML); // remove any <? ... XML Tags, PHP Parser doesnt like them.
        return $XML;
    }

    public function http_request($uri, $time_out = 10, $headers = 0)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, trim($uri));
        curl_setopt($ch, CURLOPT_HEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, $time_out);
        $result = curl_exec($ch);

        if (!curl_errno($ch)) {
            $info = curl_getinfo($ch);
            if ($info['http_code'] != '200') {
                $this->addMessage('Status Code '.$info['http_code'].' bei URL: '.$info['url'], PixiController::MessageError);
            }
        }
        curl_close($ch);

        return $result;
    }

    private function getSession($URL)
    {
        $result = $this->prepareXML($URL);
        //var_dump($result);die;
        if ($this->isXML($result) === true) {
            //var_dump($this->isXML($result));
          $XML = new SimpleXMLElement($result);

            return array($XML->SessionID, $result);
        } else {
            return 0;
        }
    }

    private function getXML($URL, $SessionID)
    {
        $URL = str_replace('$$$session$$$', $SessionID, $URL);
        $result = $this->prepareXML($URL);

        //var_dump($result);

        if ($this->isXML($result) === true) {
            $XML = new SimpleXMLElement($result);

            return array('XML' => $XML, 'File' => $result, 'URL' => $URL);
        } else {
            $this->addMessage('Error processing XML: '.$this->isXML($result), PixiController::MessageError);

            //var_dump($file);

            return array('XML' => '', 'File' => $result, 'URL' => $URL);
        }
    }

    private function checkCatalog($URL, $SessionID)
    {
        $XML_Result = $this->getXML($URL, $SessionID);

        $XML = $XML_Result['XML'];
        if (isset($XML->HEADER->CATALOG->DATABASE)) {
            //var_dump($XML); die;
        $DatabaseID = $XML->HEADER->CATALOG->DATABASE;
            $ExportDate = $XML->HEADER->CATALOG->DATEEXPORT;
      //print $ExportDAte;die;

        $itemsTable = new Table('Artikel', 'Artikels');
            $itemsTable->addColumn('SUPPLIER_AID', 'ID', DataFormat::FORMAT_STRING);
            $itemsTable->addColumn('EAN', 'EAN', DataFormat::FORMAT_STRING);
            $itemsTable->addColumn('DESCRIPTION_SHORT', 'Name', DataFormat::FORMAT_STRING);
            $itemsTable->addColumn('SEGMENT', 'Catgory', DataFormat::FORMAT_STRING);
            $itemsTable->addColumn('PRICE', 'Price', DataFormat::FORMAT_STRING);

            $itemsArray = array();
            foreach ($XML->T_NEW_CATALOG->ARTICLE as $Article) {
                $itemsArray[] = array(
          'SUPPLIER_AID' => $Article->SUPPLIER_AID,
          'EAN' => $Article->ARTICLE_DETAILS->EAN,
          'DESCRIPTION_SHORT' => $Article->ARTICLE_DETAILS->DESCRIPTION_SHORT,
          'SEGMENT' => $Article->ARTICLE_DETAILS->SEGMENT,
          'PRICE' => $Article->ARTICLE_PRICE_DETAILS->ARTICLE_PRICE->PRICE_AMOUNT,
          );
                $LastProductID = $Article->SUPPLIER_AID;
            }

            $itemsTable->addRows($itemsArray);

        //var_dump($itemsArray)

        if (
          ($DatabaseID != '') and
          (count($itemsArray) > 0)
        ) {
            return array('result' => true, 'itemsTable' => $itemsTable, 'DatabaseID' => $DatabaseID, 'ExportDate' => $ExportDate, 'LastProductID' => $LastProductID, 'URL' => $XML_Result['URL'], 'File' => $XML_Result['File']);
        } else {
            return array('result' => false, 'itemsTable' => $itemsTable, 'DatabaseID' => $DatabaseID, 'URL' => $XML_Result['URL'], 'File' => $XML_Result['File']);
        }
        } else {
            return array('result' => false, 'itemsTable' => null, 'DatabaseID' => '---', 'URL' => $XML_Result['URL'], 'File' => $XML_Result['File']);
        };
    }

    private function checkOrder($URL, $SessionID)
    {
        $XML_Result = $this->getXML($URL, $SessionID);
        $XML = $XML_Result['XML'];

        if (isset($XML->ORDER_HEADER)) {
            //print_r($XML->ORDER_HEADER->ORDER_INFO);
        //die;
          $DatabaseID = $XML->ORDER_HEADER->ORDER_INFO->DATABASE;
            $ShopID = $XML->ORDER_HEADER->ORDER_INFO->SHOPID;
            $OrderID = $XML->ORDER_HEADER->ORDER_INFO->ORDER_ID;
            $OrderDate = $XML->ORDER_HEADER->ORDER_INFO->ORDER_DATE;
        //print $OrderID;

        $itemsTable = new Table('Artikel', 'Artikels');
            $itemsTable->addColumn('LINE_ITEM_ID', 'Line-ID', DataFormat::FORMAT_STRING);
            $itemsTable->addColumn('ARTICLE_ID', 'Item-ID', DataFormat::FORMAT_STRING);
            $itemsTable->addColumn('ITEM_NAME', 'Name', DataFormat::FORMAT_STRING);
            $itemsTable->addColumn('QUANTITY', 'Qty', DataFormat::FORMAT_STRING);
            $itemsTable->addColumn('ARTICLE_PRICE', 'Price', DataFormat::FORMAT_STRING);

            $itemsArray = array();
            foreach ($XML->ORDER_ITEM_LIST->ORDER_ITEM as $Article) {
                $itemsArray[] = array(
            'LINE_ITEM_ID' => $Article->LINE_ITEM_ID,
            'ARTICLE_ID' => $Article->ARTICLE_ID->SUPPLIER_AID,
            'ITEM_NAME' => $Article->ITEM_NAME,
            'QUANTITY' => $Article->QUANTITY,
            'ARTICLE_PRICE' => $Article->ARTICLE_PRICE->PRICE_AMOUNT,
          );
            }

            $itemsTable->addRows($itemsArray);

            if (
          ($DatabaseID != '') and
          ($ShopID != '') and
          ($OrderID != '') and
            (count($itemsArray) > 0)
          ) {
                return array('result' => true,    'itemsTable' => $itemsTable,
                  'DatabaseID' => $DatabaseID,
                  'ShopID' => $ShopID,
                  'OrderID' => $OrderID,
                  'OrderDate' => $OrderDate,
                  'URL' => $XML_Result['URL'],
                  'File' => $XML_Result['File'], );
            } else {
                return array('result' => false, 'itemsTable' => $itemsTable, 'DatabaseID' => $DatabaseID, 'URL' => $XML_Result['URL'], 'File' => $XML_Result['File']);
            }
        } else {
            return array('result' => false, 'itemsTable' => null, 'DatabaseID' => '---', 'URL' => $XML_Result['URL'], 'File' => $XML_Result['File']);
        }
    }

    public function __construct()
    {
        parent::__construct();
    }

    private function setURLSession()
    {
        $URL = new stdClass();
        foreach ($_GET as $key => $item) {
            $URL->$key = $item;
        }

        $_SESSION['userinfo']['URL'] = $URL;
    }

    private function getURLSession()
    {
        //var_dump($_SESSION);
        if (isset($_SESSION['userinfo']['URL'])) {
            return  $_SESSION['userinfo']['URL'];
        } else {
            $URL = new stdClass();
            $URL->URL = 'http://localhost:8080/?pixi=';
            $URL->Authorization = 'Authorization&usr=pixi&pwd=password';
            $URL->ExportCatalog = 'ExportCatalog&session_id=$$$session$$$';
            $URL->ExportOrder = 'ExportOrder&session_id=$$$session$$$';
            $URL->ConfirmOrder = 'ConfirmOrder&session_id=$$$session$$$&orders_id=$$$order_id$$$';
            $URL->ConfirmCatalog = 'ConfirmCatalog&session_id=$$$session$$$&date=$$$date$$$';
            $URL->SendOrderChange = 'SendOrderChange&session_id=$$$session$$$&data=$$$data$$$';
            $URL->ImportStock = 'ImportStock&session_id=$$$session$$$&data=$$$data$$$';
            return $URL;
        }
    }

    public function index()
    {
        if (isset($_GET['URL'])) {
            $this->setURLSession();
        }
        $URL = $this->getURLSession();

        $URL = $this->getURLSession();
        $form = new Form('testimport', 'GET');
        $form->addElement('URL', FormElement::ElementTypeString, 'URL:', $URL->URL);
        $form->addElement('Authorization', FormElement::ElementTypeString, 'Authorization:', $URL->Authorization);
        $form->addElement('ExportCatalog', FormElement::ElementTypeString, 'Catalog Export:', $URL->ExportCatalog);
        $form->addElement('ConfirmCatalog', FormElement::ElementTypeString, 'Catalog Confirm:', $URL->ConfirmCatalog);
        $form->addElement('ExportOrder', FormElement::ElementTypeString, 'Order Export:', $URL->ExportOrder);
        $form->addElement('ConfirmOrder', FormElement::ElementTypeString, 'Order Confirm:', $URL->ConfirmOrder);
        $form->addElement('SendOrderChange', FormElement::ElementTypeString, 'Send Order Change', $URL->SendOrderChange);
        $form->addElement('ImportStock', FormElement::ElementTypeString, 'Import Stock:', $URL->ImportStock);

        $this->loadMainView('Check Import', 'Enter URLs and Calls', array($form));
    }

    public function check()
    {
        $URL = $this->getURLSession();
        if (!$URL) {
            $this->addMessage('No URLs defined.', PixiController::MessageError);
            $this->loadMainView('Check Import', 'Enter URLs and Calls', array());
            return;
        }




      // Add Info
/*
      $Overview->addElement(new InfoElement('Artikel', 'Anz: '.count($items).' Menge:'.$QtySum, 'fa-barcode', 'green', ''));
      $Overview->addElement(new InfoElement('Bestellungen', 'Anz: '.count($orders), 'fa-inbox', 'green', ''));
      $Overview->addElement(new InfoElement('1-Scan', 'Anz: '.$OneItemOrdersCount.' ('.(round(($OneItemOrdersCount/count($orders)*100),2)).'% )', 'fa-rocket', 'red', ''));
*/

      $result = $this->getSession($URL->URL.$URL->Authorization);
        $SessionID = $result[0];
        $body = '';
        $file = '';
        $itemsTable = '';
        $orderTable = '';

        if ($SessionID != '') {
            $this->addMessage('Session successfully retrieved: '.$SessionID, PixiController::MessageSuccess);
        } else {
            $this->addMessage('Session could not be retrieved.', PixiController::MessageError);
            $body .= '<h1>Session not retrieved</h1> Returned from call:
            <a href="'.$URL->URL.$URL->Authorization.'" target=_new>'.$URL->URL.$URL->Authorization.'</a>
            <hr> <TEXTAREA rows=10 cols=100>'.$result[1].'</TEXTAREA>';
        }


        if (isset($_GET['ConfirmOrder'])) {
            $ConfirmURL = str_replace('$$$order_id$$$', $_GET['ConfirmOrder'], $URL->URL.$URL->ConfirmOrder);
            $ConfirmXML = $this->getxML($ConfirmURL, $SessionID);
            if ($ConfirmXML['XML']->STATUS == 'SUCCESS') {
                $this->addMessage('Order Confirmed.'.$ConfirmXML['URL'], PixiController::MessageSuccess);
            } else {
                $this->addMessage('Order could not be confirmed.', PixiController::MessageError);
                $body .= 'Returned from Order Confirm call:<hr> <TEXTAREA rows=10 cols=100>'.$ConfirmXML['File'].'</TEXTAREA>';
            }
        }
        if (isset($_GET['ConfirmCatalog'])) {
            $ConfirmURL = str_replace('$$$date$$$', $_GET['ConfirmCatalog'], $URL->URL.$URL->ConfirmCatalog);
            $ConfirmXML = $this->getxML($ConfirmURL, $SessionID);
            if ($ConfirmXML['XML']->STATUS == 'SUCCESS') {
                $this->addMessage('Catalog Confirmed.', PixiController::MessageSuccess);
            } else {
                $this->addMessage('Catalog could not be confirmed.', PixiController::MessageError);
                $body .= 'Returned from Order Confirm call:<hr> <TEXTAREA rows=10 cols=100>'.$ConfirmXML['File'].'</TEXTAREA>';
            }
        }


        $CatalogResult = $this->checkCatalog($URL->URL.$URL->ExportCatalog, $SessionID);
        if ($CatalogResult['result'] === true) {
            $itemsTable = $CatalogResult['itemsTable'];
            $this->addMessage('Catalog retrieved. DatabaseID: '.$CatalogResult['DatabaseID'], PixiController::MessageSuccess);
            $body .= '<h1>Catalog recieved.</h1> <hr>URL to Export:  <a href="'.$CatalogResult['URL'].'" target=_new>Click to confirm - to get new articles</a><hr>';
            $body .= '<hr>Timestamp to confirm: '.$CatalogResult['ExportDate'].'  <a href="'.base_url().'index.php/testimport/check/?ConfirmCatalog='.$CatalogResult['ExportDate'].'">Click to confirm - to get new products</a>';
            $file .= '<hr>Returned Catalog from call:<hr><TEXTAREA rows=10 cols=100>'.$CatalogResult['File'].'</TEXTAREA>';
            $_SESSION['LastProductID'] = $CatalogResult['LastProductID'];
        } else {
            $ConfirmURL = str_replace('$$$date$$$', '0', $URL->URL.$URL->ConfirmCatalog);
            $ConfirmURL = str_replace('$$$session$$$', $SessionID, $ConfirmURL);
            $this->addMessage('Catalog could not be retrieved.', PixiController::MessageError);
            $body .= '<h1>Catalog not retrieved / or empty</h1> <hr> <a href="'.$CatalogResult['URL'].'" target=_new>'.$CatalogResult['URL'].'</a>';
            $body .= '<hr>Reset export:  <a href="'.base_url().'index.php/testimport/check/?ConfirmCatalog=0">Click to confirm - to get new products</a>';
            $file .= '<hr>Returned Catalog from call:<hr><TEXTAREA rows=10 cols=100>'.$CatalogResult['File'].'</TEXTAREA>';
        }

        $OrderResult = $this->checkOrder($URL->URL.$URL->ExportOrder, $SessionID);
        if ($OrderResult['result'] === true) {
            $ordersTable = $OrderResult['itemsTable'];
            $this->addMessage('Order retrieved. OrderID: '.$OrderResult['OrderID'], PixiController::MessageSuccess);
            $body .= '<hr><h1>Order recieved.</h1>';
            $file .= '<hr><A HREF="'.str_replace('$$$session$$$', $SessionID, $URL->URL.$URL->ExportOrder).'" target="_new">open xml</A>';
            $file .= 'Returned from Order call:<hr> <TEXTAREA rows=10 cols=100>'.$OrderResult['File'].'</TEXTAREA>';
            $body .= '<hr>OrderID to confirm: '.$OrderResult['OrderID'].' <a href="'.base_url().'index.php/testimport/check/?ConfirmOrder='.$OrderResult['OrderID'].'">Click to confirm - to get new order</a>';
            $_SESSION['LastOrderID'] = $OrderResult['OrderID'];
        } else {
            $ordersTable = '';
            $this->addMessage('Order could not be retrieved.', PixiController::MessageError);
            $body .= '<h1>Order not retrieved</h1><hr><a href="'.$OrderResult['URL'].'" target=_new>'.$OrderResult['URL'].'</a> ';
            $file .= '<hr>Returned from call:<hr> <TEXTAREA rows=10 cols=100>'.$OrderResult['File'].'</TEXTAREA>';
        }

        //print_r($CatalogXML);

        $this->loadMainView('Check Import', 'Enter URLs and Calls', array($body, $itemsTable, $ordersTable, $file));
    }

    public function updateOrderStatus()
    {
        $URL = $this->getURLSession();

        $body = '';
        if (isset($_GET['OrderID'])) {
            $result = $this->getSession($URL->URL.$URL->Authorization);
            $SessionID = $result[0];

            $UpdateXML = '<ORDER_ITEM><LINE_ITEM_ID>'.$_GET['OrderID'].'</LINE_ITEM_ID><ORDER_NR_EXT>'.$_GET['OrderID'].'</ORDER_NR_EXT><STATUS>'.$_GET['Status'].'</STATUS><TRACKINGID>'.$_GET['TrackingID'].'</TRACKINGID><SHIPPING_VENDOR>'.$_GET['SHIPPING_VENDOR'].'</SHIPPING_VENDOR><INVOICE_NR>'.$_GET['INVOICE_NR'].'</INVOICE_NR></ORDER_ITEM>';

            $ConfirmURL = str_replace('$$$data$$$', urlencode($UpdateXML), $URL->URL.$URL->SendOrderChange);
            $ConfirmURL = str_replace('$$$session$$$', $SessionID, $ConfirmURL);

            $body .= 'URL to change order status: <A target=_new href="'.$ConfirmURL.'">Open URL in new windows</a><hr>';
        }

        $form = new Form('testimport/updateOrderStatus', 'GET');
        $form->addElement('OrderID', FormElement::ElementTypeString, 'OrderID:');
        $form->addElement('Status', FormElement::ElementTypeString, 'Status (ANG, HAL, AUS, STO):');
        $form->addElement('TrackingID', FormElement::ElementTypeString, 'TrackingID:');
        $form->addElement('INVOICE_NR', FormElement::ElementTypeString, 'InvoiceNr:');
        $form->addElement('SHIPPING_VENDOR', FormElement::ElementTypeString, 'Carrier (ShipVendor):');

        $this->loadMainView('Check Import', 'Enter URLs and Calls', array($body, $form));
    }

    public function updateItem()
    {
        $URL = $this->getURLSession();

        $body = '';
        if (isset($_GET['ItemID'])) {
            $result = $this->getSession($URL->URL.$URL->Authorization);
            $SessionID = $result[0];

            $UpdateXML = '<ARTICLE_ITEM><ARTICLE_ITEM_ID>'.$_GET['ItemID'].'</ARTICLE_ITEM_ID><QUANTITY>'.$_GET['Qty'].'</QUANTITY><ACTIVE>'.$_GET['Active'].'</ACTIVE></ARTICLE_ITEM>';
            $ConfirmURL = str_replace('$$$data$$$', urlencode($UpdateXML), $URL->URL.$URL->ImportStock);
            $ConfirmURL = str_replace('$$$session$$$', $SessionID, $ConfirmURL);

            $body .= 'URL to change order status: <A target=_new href="'.$ConfirmURL.'">Open URL in new windows</a><hr>';
        }

        $form = new Form('testimport/updateItem', 'GET');
        $form->addElement('ItemID', FormElement::ElementTypeString, 'ItemID:');
        $form->addElement('Qty', FormElement::ElementTypeString, 'Stock QTY:');
        $form->addElement('Active', FormElement::ElementTypeString, 'Active (false, true):');

        $this->loadMainView('Check Import', 'Enter URLs and Calls', array($body, $form));
    }
    public function showSession()
    {
        var_dump($_SESSION);
    }
}
