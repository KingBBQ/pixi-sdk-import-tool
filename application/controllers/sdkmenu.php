<?php

use Pixi\Ui\Menu\Menu;
use Pixi\Ui\Menu\MenuItem;
use Pixi\Ui\UIControllerInterface;

date_default_timezone_set('CET');

class SDKMenu extends PixiController implements UIControllerInterface
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('session');
    }

    public function getAppTitle()
    {
        return 'pixi Import Tool';
    }

    /*
     * (non-PHPdoc) @see iPixiController::generateMenu()
     */
    public function generateMenu()
    {
        // create your own Menue
        // Big Demo Manu
        $mainMenu = new Menu();
        $mainMenu->addMenuItem(new MenuItem('itemimport', 'Artikelimport', 'fa-tachometer'));
        $mainMenu->addMenuItem(new MenuItem('orderimport', 'Bestellimport', 'fa-random'));
        $mainMenu->addMenuItem(new MenuItem('analyse', 'Bestell-Analyse', 'fa-flask'));
        $mainMenu->addMenuItem(new MenuItem('', 'Import Checker', 'fa-check-square-o',
        new Menu(array(
            new MenuItem('testimport', 'Settings'),
            new MenuItem('testimport/check', 'Run Check'),
            new MenuItem('testimport/updateOrderStatus', 'Update Order Status'),
            new MenuItem('testimport/updateItem', 'Update Item'),
        ))));
        $mainMenu->addMenuItem(new MenuItem('carriers', 'Logistiker Regeln', 'fa-flask'));


        return $mainMenu;
    }

    public function generateUserDropDown()
    {}

    public function generateLogDropDown()
    {}

    public function generateSearchForm()
    {}
}
